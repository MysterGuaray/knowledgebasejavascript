//*** replace: metodo para reemplazar un texto

let a="Hola mundo";
let b=a.replace('mundo','Alvaro');
//alert(b);
///////////////////////////////////////////

//*** split: metodo para separar un texto según los parametros enviados

let a1= "hola";
let b1= a1.split('l');
//alert(b1);

// si quiero dividir una palabra o frase en letras paso como parámetro un string vacio

let frase="Hola Beto";
let fraseDividida=frase.split('');
//alert(fraseDividida);

//si quiero ubicar una letra de una palabra o una frase por ejemplo vamos a ubicar el caracter 7 
//en la frase almacenada en la variable "miFrase" y nos muestra que es la letra O

let miFrase="TicTux Online";
let obtengoLetra=miFrase.split('')[7];
//alert(obtengoLetra);

//lo anterior tambien puedes hacerlo sin usar el metodo split simplemente indicando entre corchetes el indice 
//que deseas mostrar o almacenar en variable de la frase, para el sgte caso nos mostrará la letra u

let sinSplit="TicTux";
let obtengoSinSplit=sinSplit[4];
//alert(obtengoSinSplit);

//*** Metodo substring: extrae el texto a partir del indice inicial y opcionalmente se puede fijar el indice final
//el indice final no se incluye en el texto extraido pero el inicial si

let usandoSubstring="TicTux";
let extraigoTux= usandoSubstring.substring(3);
//alert(extraigoTux);

/* si el end es negativo extrae hacia atras es decir cuenta los caracteres del indice indicado como parámetro start
   y para el end sin importar el numero que asignes si es negativo a partir de ahí extrae hacia atrás  
*/

//*** Metodo substr: extrae a partir del indice solicitado el numero de caracteres en el segundo parámetro

let usandoSubstr="TicTux";
let traeme=usandoSubstr.substr(3,3);
alert(traeme);

// si el parámetro que le envias como start es negativo extrae a partir del último caracter de atrás hacia adelante

//*** Metodo slice es similar a substr cuando los parámetros son positivos
/* si el end es negativo no toma los ultimos (end) valores
    si el start es negativo comienza a contar desde el final
 */
