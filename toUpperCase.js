/* metodo tuUpperCase (convierte el texto a mayusculas) y to LowerCase(convierte el texto a minusculas)*/

let texto1="hola mundo";
let textoMayus= texto1.toUpperCase();
//alert (textoMayus);// mostrará el mensaje inicial guardado en la variable texto1 pero en mayuscula


let texto2="HOLA MUNDO";
let textoMinus= texto2.toLowerCase();
//alert (textoMinus);// mostrará el mensaje inicial guardado en la variable texto2 pero en minuscula


//ejemplo de uso: cuando requerimos verificar una respuesta correcta a un texto ingresado por un usuario:

let respuesta= prompt('¿cual es la capital de colombia?').toUpperCase().trim();
if (respuesta==='BOGOTA'){alert("es correcto");}
else{alert("respuesta incorrecta");}