/* 
Metodo Trim: nos elimina los espacios al inicio y al final de una cadena de caracteres
- no recibe parámetros
*/

let nombre='    Alvaro Cabrales   ';//asignamos espacios al inicio y al final de la cadena de caracteres
nombre=nombre.trim(); //eliminamos los espacios al inicio y al final usando trim, al mostrar el texto el resultado sera "Alvaro Cabrales"

